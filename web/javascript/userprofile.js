/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var req;
var crntUser;
var editedUser;
var selUser;

function init(){
    whoAmI();
}

function initRequest() {
    if(window.XMLHttpRequest){
        if(navigator.userAgent.indexOf('MSIE') !== -1){
            isIE=true;
        }
        return new XMLHttpRequest();
    } else if (window.ActiveXObject){
        isIE=true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

/* Updates teh 'logged in as' at the top of page and stores the current user.
 * Also hides buttons the current user is not supposed to ahve access to. */
function whoCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            crntUser=req.responseXML.childNodes;
            editedUser=req.responseXML.childNodes;
            var fname = crntUser[0].getElementsByTagName("firstName")[0].childNodes[0].nodeValue;
            var lname = crntUser[0].getElementsByTagName("lastName")[0].childNodes[0].nodeValue;
            selUser = crntUser[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue;
            document.getElementById("loginName").innerHTML=fname+" "+lname;
            if(crntUser[0].getElementsByTagName("access")[0].childNodes[0].nodeValue<2){
                //hide UI stuff
                $("#firstNameInput").hide();
                $("#fnameChngBtn").hide();
                $("#lastNameInput").hide();
                $("#lnameChngBtn").hide();
                $("#unameInput").hide();
                $("#unameChngBtn").hide();
                $("#jobChngBtn").hide();
                $("#mpUserSelect").hide();
                $("#taskManagementButton").hide();
            }
            fileStats(editedUser);
        }
    }
}

/* Starts filing the stats for editing. */
function whoIsCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            editedUser=req.responseXML.childNodes;
            fileStats(editedUser);
        }
    }
}

/* Files the selector with the data retrieved by call */
function callAllCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            fileSelections(req.responseXML);
        }
    }
}

/* Overwrites the stored sessionId before moving back to login page. */
function imOutCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            localStorage.setItem("sessionId","");
            window.location.replace("index.html");
        }
    }
}

/* Updates the user editing UI to reflect the changes made. */
function statCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            goBack(req.responseText);
            whoIs();
        }
    }
}

/* These redirect the user to login page if their session has expired. */
function toBoardCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            goBack(req.responseText);
            window.location.replace("taskboard.html");
        }
    }
}

function toTaskMCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            if(req.responseText=="TRUE")
                window.location.replace("taskmanager.html");
            } else{
                window.location.replace("index.html");
            }
    }
}

function goBack(responseText){ 
    if(responseText=="SESSION EXPIRED"){
        window.location.replace("index.html");
    }
}

/*Retrieves the data for the currently logged in user, as before. */
function whoAmI(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/View/Myself/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = whoCallback;
    req.send(null);
}

/* Retrieves the data for the user to be edited.
 * Defaults to currently logged in user on page load. */
function whoIs(){
    var uname = String(document.getElementById("mpUserSelect").value);
    if(uname.length==0){
        uname=crntUser[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue;
    }
    selUser=uname;
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/View/"+uname+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = whoIsCallback;
    req.send(null);
}

/* Retrieves all user data for filling in the user selector for managers. */
function callAllUsers(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/View/All/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = callAllCallback;
    req.send(null);
}

function signOut(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Logout/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = imOutCallback;
    req.send(null);
}

/* Checks if current session is valid before moving to desired page.
 * Expired session causes a redirect to login page. */
function returnToBoard(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Check/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = toBoardCallback;
    req.send(null);
}

function toTaskManagement(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Check/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = toTaskMCallback;
    req.send(null);
}

/* Files the data of the currently edited user to the editing UI.
 * Assigns functions to buttons depending on current user status. */
function fileStats(user){
    $("#retireButton").hide();
    $("#reinstateButton").hide();
    $("input:checkbox").attr("checked", false);
    document.getElementById("firstName").innerHTML="First Name: "+user[0].getElementsByTagName("firstName")[0].childNodes[0].nodeValue;
    document.getElementById("lastName").innerHTML="Last Name: "+user[0].getElementsByTagName("lastName")[0].childNodes[0].nodeValue;
    document.getElementById("uname").innerHTML="User Name: "+user[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue;
    document.getElementById("email").innerHTML="Email Addr.: "+user[0].getElementsByTagName("email")[0].childNodes[0].nodeValue;
    document.getElementById("phone").innerHTML="Phone Number: "+user[0].getElementsByTagName("phone")[0].childNodes[0].nodeValue;
    var jobstr;
    for(loop=0;loop<user[0].getElementsByTagName("jobs").length;loop++){
        if(user[0].getElementsByTagName("jobs")[loop]!="any") {
            jobstr=String(user[0].getElementsByTagName("jobs")[loop].childNodes[0].nodeValue);
            $("input:checkbox[value="+jobstr+"]").attr("checked", true);
        }
    }
    document.getElementById("fnameChngBtn").onclick=function(){changeUserStats("fname","firstNameInput");}; 
    document.getElementById("lnameChngBtn").onclick=function(){changeUserStats("lname","lastNameInput");};
    document.getElementById("unameChngBtn").onclick=function(){changeUserStats("uname","unameInput");};
    document.getElementById("jobChngBtn").onclick=function(){alterJobs();};
    if(crntUser[0].getElementsByTagName("access")[0].childNodes[0].nodeValue!=2){
        document.getElementById("pswChngBtn").onclick=function(){changeMyStats("psw","pswInput");};
        document.getElementById("emailChngBtn").onclick=function(){changeMyStats("email","userEmailInput");};
        document.getElementById("phoneChngBtn").onclick=function(){changeMyStats("phone","userPhoneInput");};
    } else {
        document.getElementById("pswChngBtn").onclick=function(){changeUserStats("psw","pswInput");};
        document.getElementById("emailChngBtn").onclick=function(){changeUserStats("email","userEmailInput");};
        document.getElementById("phoneChngBtn").onclick=function(){changeUserStats("phone","userPhoneInput");};
    }
    if(user[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue.toString()===crntUser[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue.toString()){
        crntUser=user;
        var fname = crntUser[0].getElementsByTagName("firstName")[0].childNodes[0].nodeValue;
        var lname = crntUser[0].getElementsByTagName("lastName")[0].childNodes[0].nodeValue;
        document.getElementById("loginName").innerHTML=fname+" "+lname;
    } else {
        if(crntUser[0].getElementsByTagName("access")[0].childNodes[0].nodeValue>=2){
            if(user[0].getElementsByTagName("access")[0].childNodes[0].nodeValue==-1){
                document.getElementById("reinstateButton").onclick=function(){changeUserAccess(1);};
                $("#reinstateButton").show();
            } else {
                document.getElementById("retireButton").onclick=function(){changeUserAccess(-1);};
                $("#retireButton").show();
            }
        }
    }
    if(crntUser[0].getElementsByTagName("access")[0].childNodes[0].nodeValue>=2){
        callAllUsers()
    }
}

/* Fills in the selector at the top of the page with user names and usernames to allow switching between users to edit.
 * Only managers can see the selector. */
function fileSelections(XML){
    var uSel = document.getElementById("mpUserSelect");
    $('#mpUserSelect').empty();
    if(XML.childNodes[0].childNodes.length >0){
            for(loop=0;loop < XML.childNodes[0].childNodes.length;loop++){
                var option = document.createElement("option");
                var user = XML.childNodes[0].childNodes[loop];
                option.value=user.getElementsByTagName("userName")[0].childNodes[0].nodeValue;
                option.innerHTML=""+user.getElementsByTagName("firstName")[0].childNodes[0].nodeValue+" "+user.getElementsByTagName("lastName")[0].childNodes[0].nodeValue;
                uSel.appendChild(option);
                }
        }
        uSel.value=selUser;
}

/* Used to modify currently logged in users data. Regular users have very limited modification rights,
 * even on the back-end. This calls to that limited service while the one below uses the broader manager-only version. */
function changeMyStats(what,where){
    var how;
    if(String(what)=="psw"){
        if(!(String(document.getElementById(where).value)==String(document.getElementById("rePswInput").value))){
            document.getElementById("psw").innerHTML="Password - Fields Mismatched";
            return;
        } else {
            how = String(document.getElementById(where).value);
            document.getElementById("rePswInput").value='';
            document.getElementById("psw").innerHTML="Password - Changed";
        }
    } else{
        how = String(document.getElementById(where).value);
        document.getElementById("psw").innerHTML="Password";
    }
    document.getElementById(where).value='';
    if(how.length>1){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Profile/EditProfileSelf/"+what+"/"+how+"/"+localStorage.getItem("sessionId");
    how.value=null;
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = statCallback;
    req.send(null);
    }
}

/* Modifies any users data. For password, checks that you retyped it correctly before changing it.
 * Does not send the request if the field for the new value is empty. */
function changeUserStats(what,where){
    var how;
    if(what=="psw"){
        if(!(String(document.getElementById(where).value)==String(document.getElementById("rePswInput").value))){
            document.getElementById("psw").innerHTML="Password - Fields Mismatched";
            return;
        } else {
            how = String(document.getElementById(where).value);
            document.getElementById("rePswInput").value='';
            document.getElementById("psw").innerHTML="Password - Changed";
        }
    } else{
        how = String(document.getElementById(where).value);
        document.getElementById("psw").innerHTML="Password";
    }
    document.getElementById(where).value='';
    if(how.length>1){
    var who = editedUser[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue;
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Profile/EditProfileOther/"+who+"/"+what+"/"+how+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = statCallback;
    req.send(null);
    }
}

/* Modifies user access level: 1 regular, 2 manager, 0> deactivated */
function changeUserAccess(newAccess){
    var who = editedUser[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue;
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Profile/EditProfileOther/"+who+"/access/"+newAccess+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = statCallback;
    req.send(null);
}

/* Gathers the job values with checked checkboxes, then sends the new job 'list' String to the back-end resource. 
 * Since the user will always have job 'any', guarding against incomplete URI is not necessary. */
function alterJobs(){
    var jobboxes = [];
    $('#jobdiv input:checked').each(function() {jobboxes.push(this.value);});
    var jobs ="";
    for(loop=0;loop < jobboxes.length;loop++){
        jobs=jobs+jobboxes[loop].toLowerCase();
        $("input:checkbox[value="+jobboxes[loop].toLowerCase()+"]").attr("checked", false);
        jobs=jobs+"&";
    }
    jobs=jobs+"any";
    var who = editedUser[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue;
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Profile/EditProfileOther/"+who+"/jobs/"+jobs+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = statCallback;
    req.send(null);
}