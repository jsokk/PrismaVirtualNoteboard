/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Sets up some global variables, wires a few buttons, cleans the forms and begins the initial data retrieval calls */
$(document).ready(function(){
    $("#cancelButton").click(function(){
        $("#createTaskForm").hide();
    });

    $("#newTaskButton").click(function(){
        $("#createTaskForm").show();
    });

    $("#createButton").click(function(){
        $("#createTaskForm").hide();
    });
    
    $("#cancelAddButton").click(function(){
        $("#addUserForm").hide();
    });

    $("#addNewUserButton").click(function(){
        $("#addUserForm").show();
    });

    $("#addButton").click(function(){
        $("#addUserForm").hide();
    });
    toDoList = document.getElementById("toDoDiv");
    inProgList = document.getElementById("inProgDiv");
    compList = document.getElementById("completeDiv");
    toDoHeader = document.getElementById("toDoHeader");
    inProgHeader = document.getElementById("inProgHeader");
    completeHeader = document.getElementById("completeHeader");
    whoAmI(); 
    prepAddForm();
    prepCreateForm();
});

var req;
var crntUser;
var myJobs;

var toDoList;
var inProgList;
var compList;
var toDoHeader;
var inProgHeader;
var completeHeader;

function initRequest() {
    if(window.XMLHttpRequest){
        if(navigator.userAgent.indexOf('MSIE') !== -1){
            isIE=true;
        }
        return new XMLHttpRequest();
    } else if (window.ActiveXObject){
        isIE=true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

/* Callback functions for the REST requests, grouped together */

function creationCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            goBack(req.responseText)
            prepCreateForm();
            if(req.responseText !== "FAILURE") {
                alert("New task added sucessfully!");
            }
        }
        if(req.status == 404){
                alert("Addition fail, try again!")
        }
    }
}

function addUserCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            goBack(req.responseText)
            prepAddForm();
            if(req.responseText !== "FAILURE") {
                alert("New user added sucessfully!");
            }
        }
        if(req.status == 404){
                alert("Addition fail, try again!")
        }
    }
}

function inProgressCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            fileInProgress(req.responseXML);
        }
    }
}

function getToDoCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            fileToDo(req.responseXML);
        }
    }
}

function getCompleteCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            fileComplete(req.responseXML);
        }
    }
}

function checkoutCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            goBack(req.responseText)
        }
        retrieveAll();
    }
}

function doneDropCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            goBack(req.responseText)
        }
        retrieveAll();
    }
}

/* Receives the data for the current user, used to determine viewing and creation rights */
function whoCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            crntUser=req.responseXML.childNodes;
            var fname = crntUser[0].getElementsByTagName("firstName")[0].childNodes[0].nodeValue;
            var lname = crntUser[0].getElementsByTagName("lastName")[0].childNodes[0].nodeValue;
            document.getElementById("loginName").innerHTML=fname+" "+lname;
            myJobs = [];
            for(loop=0;loop<crntUser[0].getElementsByTagName("jobs").length;loop++){
                myJobs.push(String(crntUser[0].getElementsByTagName("jobs")[loop].childNodes[0].nodeValue));
            }
            if(crntUser[0].getElementsByTagName("access")[0].childNodes[0].nodeValue<2){
                //hide UI stuff
                $("#addNewUserButton").hide();
                $("#newTaskButton").hide();
                $("#taskManagementButton").hide();
            }
            retrieveAll();
        }
    }
}

/* Overwrites the sessionId token (which would no longer be valid to the server) before returning to login page */
function imOutCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            localStorage.setItem("sessionId","");
            window.location.replace("index.html");
        }
    }
}

/* An expired session redirects to login instead of the desired page */
function toMyProfileCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            if(req.responseText=="TRUE")
                window.location.replace("userprofile.html");
            } else{
                window.location.replace("index.html");
            }
        }
}

/* An expired session redirects to login instead of the desired page */
function toTaskMCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            if(req.responseText=="TRUE")
                window.location.replace("taskmanager.html");
            } else{
                window.location.replace("index.html");
            }
    }
}

/* A function called by other callbacks to check session expiration */
function goBack(responseText){ 
    if(responseText=="SESSION EXPIRED"){
        window.location.replace("index.html");
    }
}

/* Composes the REST call for task creation from the input retrieved from User Creation form 
 * User is notified of incomplete input by the callback as the REST calls fails due to a '//' not corresponding to any valid REST URIs 
 * It is debatable which is better: checking all params for length>0 or allowing the REST call to fail harmlessly */
function createTask(){
    var crtname = document.getElementById("taskname");
    var crtdesc = document.getElementById("description");
    var location = document.getElementById("location");
    var duedate = document.getElementById("dueDate").value;
    var duetime = document.getElementById("dueTime").value;
    var tasktype=$('.crtcheckbox:checked').val();
    
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/Create/"+crtname.value+"/"+crtdesc.value+"/"+location.value+"/"+tasktype+"/"+duedate+"T"+duetime+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("POST",url,true);
    req.onreadystatechange = creationCallback;
    req.send(null);
}

/* Composes the REST call for user creation from the input retrieved from User Creation form 
 * User is notified of incomplete input by the callback as the REST calls fails due to a '//' not corresponding to any valid REST URIs */
function createUser(){
    var fname = document.getElementById("firstName").value;
    var lname = document.getElementById("lastName").value;
    var uname = document.getElementById("uname").value;
    var psw = document.getElementById("psw").value;
    var repsw = document.getElementById("rePsw").value;
    var email = document.getElementById("userEmail").value;
    var phone = document.getElementById("userPhone").value;
    var access = 1;
    var jobboxes = [];
    $('#jobdiv input:checked').each(function() {jobboxes.push(this.value);});
    var jobs ="";
    for(loop=0;loop < jobboxes.length;loop++){
        if(jobboxes[loop].toLowerCase()=="manager"){
            access=2;
        }
        jobs=jobs+jobboxes[loop].toLowerCase();
        jobs=jobs+"&";
    }
    jobs=jobs+"any";
    if(psw==repsw){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Add/"+fname+"/"+lname+"/"+uname+"/"+psw+"/"+email+"/"+phone+"/"+access+"/"+jobs+"/"+localStorage.getItem("sessionId");
    req=initRequest(); 
    req.open("POST",url,true);
    req.onreadystatechange = addUserCallback;
    req.send(null);
    }
}

/* Empties the Task Creation form */
function prepCreateForm(){
    var crntDate = new Date();
    var crntM = crntDate.getMonth()+1;
    if(crntM<10){
        crntM="0"+crntM;
    }
    var crntD =crntDate.getDate();
    if(crntD<10){
        crntD="0"+crntD;
    }
    var date = ""+crntDate.getFullYear()+"-"+crntM+"-"+crntD;
    document.getElementById("taskname").value = null;
    document.getElementById("description").value = null;
    document.getElementById("location").value = null;
    document.getElementById("dueDate").value = date;
    document.getElementById("dueTime").value = "15:00";
}

/* Empties out the 'Add User' form between uses
 * Only called on successful use, so you do not lose work to mistakes*/
function prepAddForm(){
    document.getElementById("firstName").value=null;
    document.getElementById("lastName").value=null;
    document.getElementById("uname").value=null;
    document.getElementById("psw").value=null;
    document.getElementById("rePsw").value=null;
    document.getElementById("userEmail").value=null;
    document.getElementById("userPhone").value=null;
    var checks = document.getElementsByClassName("jobcheckbox prisma-task-category");
    for(loop=0;loop<checks.length;loop++){
        checks[loop].checked = false;
    }
}

/* Various REST resource calls used to retrieve the information served to the user
 * Managers have greater viewing rights than regular users 
 * This is handled by sending a different request, based off of the data from the 'WhoAmI()' call 
 * The requests for the tasks are called in sequence: To Do -> In Progress -> Complete */

function retrieveToDo(){
    if(crntUser[0].getElementsByTagName("access")[0].childNodes[0].nodeValue>=2){
        var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/ByStatus/0/"+localStorage.getItem("sessionId");
    } else {
        var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/ForMe/ByStatus/0/"+localStorage.getItem("sessionId");
    }
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = getToDoCallback;
    req.send(null);
}

function retrieveComplete(){
    if(crntUser[0].getElementsByTagName("access")[0].childNodes[0].nodeValue>=2){
        var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/ByStatus/2/"+localStorage.getItem("sessionId");
    } else {
        var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/ForMe/ByStatus/2/"+localStorage.getItem("sessionId");
    }
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = getCompleteCallback;
    req.send(null);
}

function retrieveAll(){
    retrieveToDo();
}

function retrieveInProgress(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/ForMe/ByStatus/1/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = inProgressCallback;
    req.send(null);
    
}

function checkoutTask(taskId){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/Checkout/"+taskId+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = checkoutCallback;
    req.send(null);
}

function dropTask(taskId){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/Drop/"+taskId+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = doneDropCallback;
    req.send(null);
}

function completeTask(taskId){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/Complete/"+taskId+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = doneDropCallback;
    req.send(null);
}

function whoAmI(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/View/Myself/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = whoCallback;
    req.send(null);
}

/* Tells the server to remove the sessionId from the valid sessionIds 
 * Returning to login page and overwriting the sessionId is done once the server confirms it removed your session */
function signOut(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Logout/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = imOutCallback;
    req.send(null);
}

/* Moving between pages makes a check to see if your session is still valid.
 * If it timed out you return to login page.
 * This is to safeguard against people forgetting they are signed in.*/
function goToMyProfile(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Check/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = toMyProfileCallback;
    req.send(null);
}

/* As above */
function toTaskManagement(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Check/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = toTaskMCallback;
    req.send(null);
}

/* Used to empty the task note tabs, called at the start of filing in tasks */
function clearDiv(div){
        for (loop = div.childNodes.length -1; loop>=0;loop--){
            div.removeChild(div.childNodes[loop]);
        }
}

/* Reformats the LocalDateTime.toString() received from the backend into an user-friendlier shape */
function remasterTime(duedate){  //"yyyy-mm-ddThh:mm" ->0123-56-89T[11][12]:[14][15]
    var day;
    if(duedate[8]==1){
        day=duedate[8]+duedate[9]+"th";
    } else{
        if(duedate[9]==1){
            day="1st";
        } else if(duedate[9]==2){
            day="2nd";
        } else if(duedate[9]==3){
            day="3rd";
        } else{
            day=duedate[9]+"th";
        }
        if(duedate[8]>0){
        day=duedate[8]+day;
        }
    }
    day=duedate[8]+duedate[9];
    var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
    var monthNumber = duedate[5]+duedate[6];
    var month = monthNames[parseInt(monthNumber)-1];
    month = duedate[5]+duedate[6];
    var newtime = duedate[11]+duedate[12]+":"+duedate[14]+duedate[15]+" - "+day+"."+month;
    return newtime;
}

/* Fills in the full 'To Do'-task description popup with the information stored in the anonomyous function (see 'addTaskToDo()') */
function fileFull(title,crttime,creator,type,desc,loc,duetime,id){
    document.getElementById("fulltitle").innerHTML=title; //full-title,full-time-creator,full-type,full-desc,full-time,full-loc fullcheck
    document.getElementById("fulltimecreator").innerHTML="Posted "+remasterTime(crttime)+"<br/>by "+creator;
    document.getElementById("fulltype").innerHTML=type;
    document.getElementById("fulldesc").innerHTML=desc;
    document.getElementById("fulltime").innerHTML="<span class='glyphicon glyphicon-time'></span><br/>"+remasterTime(duetime); 
    document.getElementById("fullloc").innerHTML="<span class='glyphicon glyphicon-map-marker'></span><br/>"+loc;
    if($.inArray(type, myJobs) > -1){
        document.getElementById("fullcheck").onclick= function() {checkoutTask(id)};
        $("#fullcheck").show();
    } else {
        $("#fullcheck").hide();
    }
    document.getElementById('full-task').style.display='block';
}

/* Files all 'To Do' tasks into task notes. 'To Do' contains tasks that nobody has yet picked up.
 * Regular users can only see tasks they are qualified to take while managers see all open tasks.
 * Managers still cannot claim tasks outside their own job list, by design.
 * Managers can modify their own job settings, so allowing them to pick  'outside their expertise' would only be enabling misclicks.*/
function fileToDo(responseXML){
    clearDiv(toDoList);
    if(responseXML == null){
        return false;
    } else {
        if(responseXML.childNodes==null){}
        else{
            toDoHeader.innerHTML="To do - "+responseXML.childNodes[0].childNodes.length+" item(s)";
        if(responseXML.childNodes[0].childNodes.length >0){
            for(loop=0;loop < responseXML.childNodes[0].childNodes.length;loop++){
                var task = responseXML.childNodes[0].childNodes[loop]; 
                var title = task.getElementsByTagName("taskTitle")[0];
                var desc = task.getElementsByTagName("taskDescF")[0];
                var duedate = task.getElementsByTagName("dueDate")[0];
                var location = task.getElementsByTagName("location")[0];
                var creator = task.getElementsByTagName("creator")[0];
                var crttime = task.getElementsByTagName("creationTime")[0]
                var taskType = task.getElementsByTagName("taskType")[0];
                var taskId = task.getElementsByTagName("id")[0];
                addTaskToDo(title.childNodes[0].nodeValue,
                    desc.childNodes[0].nodeValue,
                    duedate.childNodes[0].nodeValue,
                    location.childNodes[0].nodeValue,
                    creator.childNodes[0].nodeValue,
                    crttime.childNodes[0].nodeValue,
                    taskType.childNodes[0].nodeValue,
                    taskId.childNodes[0].nodeValue)
                    ;
                }
            }
        toDoHeader.innerHTML="To do ("+responseXML.childNodes[0].childNodes.length+")";
        retrieveInProgress();
        }
    }
}

/* Creates a task note for a 'To Do' task and sets up the function to update the popup to this task on click.
 * 'The popup is the same for all task notes, they simply switch in their own information when clicked.*/
function addTaskToDo(title,desc,duedate,location,creator,crttime,taskType,id){
    var taskshell = document.createElement("div");
    taskshell.className = "col-lg-3 col-md-4 col-sm-6 col-xs-12";
    var d1 = document.createElement("div");
    d1.className="prisma-task"; 
    d1.onclick=function(){fileFull(title,crttime,creator,taskType,desc,location,duedate,id)};
    taskshell.appendChild(d1);
    var d2 = document.createElement("div");
    d2.className="prisma-task-header";
    d1.appendChild(d2);
    var h3 = document.createElement("h3");
    h3.className="prisma-task-title";
    if(title.length<20){
        h3.innerHTML=title;
    } else {
        h3.innerHTML=title.substring(0,17)+"...";
    }
    d2.appendChild(h3);
    var d3 = document.createElement("div");
    d1.appendChild(d3);
    d3.className="prisma-task-body row";
    var d4 = document.createElement("div");
    d4.className="col-xs-12";
    d3.appendChild(d4);
    var d5 = document.createElement("div");
    d5.className="prisma-task-deadline col-xs-12";
    //var span = document.createElement("span");
    //span.className="glyphicon glyphicon-time";
    //d5.appendChild(span);
    d5.innerHTML="<span class='glyphicon glyphicon-time'></span>"+remasterTime(duedate);
    d4.appendChild(d5);
    var d6 = document.createElement("div");
    d6.className="prisma-task-location col-xs-12";
    //var span = document.createElement("span");
    //span.className="glyphicon glyphicon-map-marker"; "<span class='lyphicon glyphicon-map-marker'></span>"+
    //d6.appendChild(span);
    d6.innerHTML="<span class='glyphicon glyphicon-map-marker'></span>"+location;
    d4.appendChild(d6);
    var d7 = document.createElement("div");
    d7.className="prisma-task-categories col-xs-12";
    d4.appendChild(d7);
    var ul = document.createElement("ul");
    d7.appendChild(ul);
    var span = document.createElement("span");
    span.className="glyphicon glyphicon-menu-hamburger";
    ul.appendChild(span);
    var li1 = document.createElement("li");
    li1.className="prisma-task-category";
    li1.innerHTML=taskType;
    ul.appendChild(li1);
    var d8=document.createElement("div");
    d8.className="pull-right";
    d8.innerHTML="Published by "+creator;
    d3.appendChild(d8);
    toDoList.appendChild(taskshell);
}

/* Files all completed tasks into tasknotes. These are not interactable.
 * Regular users will see tasks they themselves completed. Managers will see all completed tasks.
 * The tab is emptied and refilled every time it needs updating. 
 * Regular users will not notice but for a very large store a manager could theoretically see some slowdown.*/
function fileComplete(responseXML){
    clearDiv(compList);
    var count = 0;
    if(responseXML == null){
        
        return false;
    } else {
        if(responseXML.childNodes[0].childNodes.length >0){
            for(loop=0;loop < responseXML.childNodes[0].childNodes.length;loop++){
                var task = responseXML.childNodes[0].childNodes[loop]; 
                var title = task.getElementsByTagName("taskTitle")[0];
                var desc = task.getElementsByTagName("taskDescF")[0];
                var duedate = task.getElementsByTagName("dueDate")[0];
                var location = task.getElementsByTagName("location")[0];
                var creator = task.getElementsByTagName("creator")[0];
                var taskType = task.getElementsByTagName("taskType")[0];
                var user = task.getElementsByTagName("user")[0];
                if(crntUser[0].getElementsByTagName("access")[0].childNodes[0].nodeValue>=2 || crntUser[0].getElementsByTagName("userName")[0].childNodes[0].nodeValue==user.childNodes[0].nodeValue){ //
                addComplete(title.childNodes[0].nodeValue,
                    desc.childNodes[0].nodeValue,
                    duedate.childNodes[0].nodeValue,
                    location.childNodes[0].nodeValue,
                    creator.childNodes[0].nodeValue,
                    taskType.childNodes[0].nodeValue,
                    user.childNodes[0].nodeValue);
                count++
                }
            }
            }
        completeHeader.innerHTML="Complete ("+count+")";
    }
}

/* Creates a tasknote for a completed task */
function addComplete(title,desc,duedate,location,creator,taskType,completer){
    var taskshell = document.createElement("div");
    taskshell.className = "col-lg-3 col-md-4 col-sm-6 col-xs-12";
    var d1 = document.createElement("div");
    d1.className="prisma-task"; 
    taskshell.appendChild(d1);
    var d2 = document.createElement("div");
    d2.className="prisma-task-header";
    d1.appendChild(d2);
    var h3 = document.createElement("h3");
    h3.className="prisma-task-title";
    if(title.length<20){
        h3.innerHTML=title;
    } else {
        h3.innerHTML=title.substring(0,17)+"...";
    }
    d2.appendChild(h3);
    var d3 = document.createElement("div");
    d1.appendChild(d3);
    d3.className="prisma-task-body row";
    var d4 = document.createElement("div");
    d4.className="col-xs-12";
    d3.appendChild(d4);
    var d5 = document.createElement("div");
    d5.className="prisma-task-deadline col-xs-12";
    //var span = document.createElement("span");
    //span.className="glyphicon glyphicon-time";
    //d5.appendChild(span);
    d5.innerHTML="<span class='glyphicon glyphicon-time'></span>"+remasterTime(duedate);
    d4.appendChild(d5);
    var d6 = document.createElement("div");
    d6.className="prisma-task-location col-xs-12";
    //var span = document.createElement("span");
    //span.className="glyphicon glyphicon-map-marker"; "<span class='lyphicon glyphicon-map-marker'></span>"+
    //d6.appendChild(span);
    d6.innerHTML="<span class='glyphicon glyphicon-user'></span> Completed by"+completer;
    d4.appendChild(d6);

    var d7 = document.createElement("div");
    d7.className="prisma-task-categories col-xs-12";
    d4.appendChild(d7);
    var ul = document.createElement("ul");
    d7.appendChild(ul);
    var span = document.createElement("span");
    span.className="glyphicon glyphicon-menu-hamburger";
    ul.appendChild(span);
    var li1 = document.createElement("li");
    li1.className="prisma-task-category";
    li1.innerHTML=location;
    ul.appendChild(li1);
    var li2 = document.createElement("li");
    li2.className="prisma-task-category";
    li2.innerHTML=taskType;
    ul.appendChild(li2);
    var d8=document.createElement("div");
    d8.className="pull-right";
    d8.innerHTML="Published by "+creator;
    d3.appendChild(d8);
    compList.appendChild(taskshell);
}

/* Files all 'in progress' tasks into interactable task notes. 
 * 'In progress' tasks are ones the currently logged in user has checked out,
 * indicating that they intend to complete that task.
 * The tab for 'In progress' tasks is emptied and refilled every time it is refreshed 
 * for simplicitys sake.*/
function fileInProgress(responseXML){
    clearDiv(inProgList);
    if(responseXML == null){
        return false;
    } else {
        if(responseXML.childNodes==null){}
        else{
            inProgHeader.innerHTML="In progress ("+responseXML.childNodes[0].childNodes.length+")";
        if(responseXML.childNodes[0].childNodes.length >0){
            for(loop=0;loop < responseXML.childNodes[0].childNodes.length;loop++){
                var task = responseXML.childNodes[0].childNodes[loop];
                var title = task.getElementsByTagName("taskTitle")[0];
                var duedate = task.getElementsByTagName("dueDate")[0];
                var location = task.getElementsByTagName("location")[0];
                var taskId = task.getElementsByTagName("id")[0];
                addTaskInProg(title.childNodes[0].nodeValue,
                    duedate.childNodes[0].nodeValue,
                    location.childNodes[0].nodeValue,
                    taskId.childNodes[0].nodeValue);
            }
        }
        inProgHeader.innerHTML="In progress ("+responseXML.childNodes[0].childNodes.length+")";
        retrieveComplete();
    }
}
}

/* Creates a task note for an 'in progress' task */
function addTaskInProg(title,duedate,location,id){
    var taskshell = document.createElement("div");
    taskshell.className = "col-lg-3 col-md-4 col-sm-6 col-xs-12";
    var d1 = document.createElement("div");
    d1.className=" prisma-task";
    taskshell.appendChild(d1);
    var d2 = document.createElement("div");
    d2.className="prisma-task-header";
    d1.appendChild(d2);
    var h3 = document.createElement("h3");
    h3.className="prisma-task-title";
    if(title.length<20){
        h3.innerHTML=title;
    } else {
        h3.innerHTML=title.substring(0,17)+"...";
    }
    d2.appendChild(h3);
    var d3 = document.createElement("div");
    d1.appendChild(d3);
    d3.className="prisma-task-body row centered";
    var d4 = document.createElement("div");
    d4.className="prisma-task-deadline";
    var span = document.createElement("span");
    span.className="glyphicon glyphicon-time";
    d4.innerHTML="<span class='glyphicon glyphicon-time'></span><br>"+remasterTime(duedate);
    d3.appendChild(d4);
    var d5 = document.createElement("div");
    d5.className="prisma-task-location";
    var span = document.createElement("span");
    span.className="glyphicon glyphicon-map-marker";
    d5.innerHTML="<span class='glyphicon glyphicon-map-marker'></span><br>"+location;;
    d3.appendChild(d5);
    var d6 = document.createElement("div");
    d6.className="prisma-task-buttons col-xs-12";
    d3.appendChild(d6);
    var a1 = document.createElement("a");
    a1.className="btn-danger prisma-button-drop";
    a1.onclick= function() {dropTask(id)};
    a1.innerHTML="Drop";
    d6.appendChild(a1);
    var a2 = document.createElement("a");
    a2.className="btn-primary prisma-button-done";
    a2.onclick= function() {completeTask(id)};
    a2.innerHTML="Done";
    d6.appendChild(a2);
    inProgList.appendChild(taskshell);
}