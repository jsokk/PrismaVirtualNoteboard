/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var unameField;
var pswField;
var req;

/* Non-repeateble populate is called on load to ensure the app is ready to test/demo without extra steps. */
function init(){
    document.getElementById('prisma-login').style.display='block';
    unameField = document.getElementById("uname");
    pswField = document.getElementById("psw");
    populate();
}

function initRequest() {
    if(window.XMLHttpRequest){
        if(navigator.userAgent.indexOf('MSIE') !== -1){
            isIE=true;
        }
        return new XMLHttpRequest();
    } else if (window.ActiveXObject){
        isIE=true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

/* Sends the username and password entered to the server for verification. */
function login(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Login/"+unameField.value+"/"+pswField.value;
    console.log((unameField.value)+"/"+(pswField.value));
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = loginCallback;
    req.send(null);
    unameField.value="";
    pswField.value="";
}

/* Stores your sessionId and moves to taskboard if your login was valid, displays an alert otherwise. */
function loginCallback(){
    
    if (req.readyState == 4){
        if(req.status == 200){
            if(req.responseText!=="FAILURE"){
                localStorage.setItem("sessionId",req.responseText);
                window.location.replace("taskboard.html");
            }else {
                alert("Your username and password are invalid.")
            }
        }
    }
}
/*Populates the server with users and tasks if this has not already been done (server drops repeat requests without 'force') */
function populate(){
    console.log("pop");
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/populate/setup";
    req=initRequest();
    req.open("GET",url,true);
    req.send(null);
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/populate/setup";
    req=initRequest();
    req.open("GET",url,true);
    req.send(null);
    localStorage.setItem("populate","done");
}

/* Resets testing/demo users to their initial status. */
function populateUsers(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/populate/force";
    req=initRequest();
    req.open("GET",url,true);
    req.send(null);
}

/* Resets testing/demo tasks to their initial status. */
function populateTasks(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/populate/force";
    req=initRequest();
    req.open("GET",url,true);
    req.send(null);
    }