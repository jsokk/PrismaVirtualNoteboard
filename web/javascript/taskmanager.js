/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* making some trivial commenting changes to allow a new Add/Commit/Push */
var req;
var crntUser;
var allUsers;
var revDesc;
var crnTask;

/* Kicks off the chain of data requests to fill in the UI. */
function init(){
    whoAmI();
}

function initRequest() {
    if(window.XMLHttpRequest){
        if(navigator.userAgent.indexOf('MSIE') !== -1){
            isIE=true;
        }
        return new XMLHttpRequest();
    } else if (window.ActiveXObject){
        isIE=true;
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

/* Callbacks for REST requests, grouped together*/

/*Sets the 'logged in as' at the top*/
function whoCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            crntUser=req.responseXML.childNodes;
            var fname = crntUser[0].getElementsByTagName("firstName")[0].childNodes[0].nodeValue;
            var lname = crntUser[0].getElementsByTagName("lastName")[0].childNodes[0].nodeValue;
            document.getElementById("loginName").innerHTML=fname+" "+lname;
            
            callAllUsers();
        }
    }
}

function whoIsCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            editedUser=req.responseXML.childNodes;
            fileStats(editedUser);
        }
    }
}

function callAllUsersCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            allUsers = req.responseXML;
            callAllTasks();
        }
    }
}

function callAllTasksCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            fileTaskSelect(req.responseXML);
        }
    }
}

function getTaskCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            fileTask(req.responseXML);
        }
    }
}

function taskModCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            callAllTasks();
        }
    }
}

/* 'imOut' empties the sessionId before returning to login.
 * Other page changers check the validity of your sessionId before allowing you to go anywhere other than login. */
function imOutCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            localStorage.setItem("sessionId","");
            window.location.replace("index.html");
        }
    }
}

function toBoardCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            goBack(req.responseText);
            window.location.replace("taskboard.html");
        }
    }
}

function goBack(responseText){ 
    if(responseText=="SESSION EXPIRED"){
        window.location.replace("index.html");
    }
}

function toMyProfileCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            if(req.responseText=="TRUE")
            window.location.replace("userprofile.html");
        } else{
        window.location.replace("index.html");
    }
    }
}


function toTaskMCallback(){
    if (req.readyState == 4){
        if(req.status == 200){
            if(req.responseText=="TRUE")
            window.location.replace("taskmanager.html");
        } else{
        window.location.replace("index.html");
        }
    }
}


/* REST resource calls */
function whoAmI(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/View/Myself/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = whoCallback;
    req.send(null);
}

function callAllUsers(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/View/All/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = callAllUsersCallback;
    req.send(null);
}

function callAllTasks(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/All/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = callAllTasksCallback;
    req.send(null);
}

function getTask(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/All/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("GET",url,true);
    req.onreadystatechange = getTaskCallback;
    req.send(null);
}

function signOut(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Logout/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = imOutCallback;
    req.send(null);
}

function returnToBoard(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Check/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = toBoardCallback;
    req.send(null);
}

function goToMyProfile(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Check/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = toMyProfileCallback;
    req.send(null);
}

function toTaskManagement(){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Users/Check/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = toTaskMCallback;
    req.send(null);
}

/* Files in task names into the selector and stores their taskId with them so their data can be retrieved when you change between tasks. */
function fileTaskSelect(Tasks){
    var tSel = document.getElementById("tmTaskSelect");
    $('#tmTaskSelect').empty();
    if(Tasks.childNodes[0].childNodes.length >0){
            $('#tmTaskSelect').empty();
            for(loop=0;loop < Tasks.childNodes[0].childNodes.length;loop++){
                var option = document.createElement("option");
                var task = Tasks.childNodes[0].childNodes[loop];
                option.value=task.getElementsByTagName("id")[0].childNodes[0].nodeValue;
                option.innerHTML=""+task.getElementsByTagName("taskTitle")[0].childNodes[0].nodeValue;
                tSel.appendChild(option);
            }
            if(tSel.value.length<=0){
                tSel.value=Tasks.childNodes[0].childNodes[0].getElementsByTagName("id")[0].childNodes[0].nodeValue;
            }
            fileTask(Tasks);
        }
}

/* Files the currently viewed task into the UI and toggles editing button visibility.
 * Completed tasks can only be deleted or reopened. Only non-complete tasks can be edited.
 * A completed task needs to be reopened if you wish to edit it. */
function fileTask(tasks){
    $('#titleChngBtn').show();
    $('#descChngBtn').show();
    $('#locChngBtn').show();
    $('#userChngBtn').show();
    $('#typeChngBtn').show();
    $('#dateChngBtn').show();
    $('#timeChngBtn').show();
    $('#reopenButton').show();
    var tSel = document.getElementById("tmTaskSelect");
    crnTask=tSel.value;
    for(loop=0;loop < tasks.childNodes[0].childNodes.length;loop++){
        var task = tasks.childNodes[0].childNodes[loop];
        var id =task.getElementsByTagName("id")[0].childNodes[0].nodeValue;
        if(id ==tSel.value){
            $("#retireButton").hide();
            document.getElementById("title").innerHTML="Title: "+task.getElementsByTagName("taskTitle")[0].childNodes[0].nodeValue;
            document.getElementById("description").innerHTML=task.getElementsByTagName("taskDescF")[0].childNodes[0].nodeValue;
            revDesc=String(task.getElementsByTagName("taskDescF")[0].childNodes[0].nodeValue);
            document.getElementById("location").innerHTML="Location: "+task.getElementsByTagName("location")[0].childNodes[0].nodeValue;
            var uSel = document.getElementById("tmUserSelect");
            $('#tmUserSelect').empty();
            var option = document.createElement("option");
            option.value='none';
            option.innerHTML='None';
            uSel.appendChild(option);
            for(loop=0;loop<allUsers.childNodes[0].childNodes.length;loop++){
                var user = allUsers.childNodes[0].childNodes[loop];
                for(loop2=0;loop2<user.getElementsByTagName("jobs").length;loop2++){
                    if(user.getElementsByTagName("jobs")[loop2].childNodes[0].nodeValue==task.getElementsByTagName("taskType")[0].childNodes[0].nodeValue){
                        var option = document.createElement("option");
                        option.value=user.getElementsByTagName("userName")[0].childNodes[0].nodeValue;
                        option.innerHTML=""+user.getElementsByTagName("firstName")[0].childNodes[0].nodeValue+" "+user.getElementsByTagName("lastName")[0].childNodes[0].nodeValue;
                        uSel.appendChild(option);
                    }
                }
            }
            if(task.getElementsByTagName("user").length>0){
                uSel.value=task.getElementsByTagName("user")[0].childNodes[0].nodeValue;
            }
            document.getElementById("dueDate").value = task.getElementsByTagName("dueDate")[0].childNodes[0].nodeValue.substr(0, 10);
            document.getElementById("dueTime").value = task.getElementsByTagName("dueDate")[0].childNodes[0].nodeValue.substr(11, 15);
            document.getElementById("tmTypeSelect").value=task.getElementsByTagName("taskType")[0].childNodes[0].nodeValue;
            document.getElementById("descRevertBtn").onclick=function(){$('#description').val(revDesc);};
            
            document.getElementById("titleChngBtn").onclick=function(){modifyTask("title", "titleInput",id);};
            document.getElementById("descChngBtn").onclick=function(){modifyTask("description", "description",id);};
            document.getElementById("locChngBtn").onclick=function(){modifyTask("location", "locationInput",id);};
            document.getElementById("userChngBtn").onclick=function(){modifyTask("user", "tmUserSelect",id);};
            document.getElementById("typeChngBtn").onclick=function(){modifyTask("tasktype", "tmTypeSelect",id);};
            document.getElementById("dateChngBtn").onclick=function(){modifyTaskTime(id);};
            document.getElementById("timeChngBtn").onclick=function(){modifyTaskTime(id);};
            document.getElementById("deleteButton").onclick=function(){removeTask(id);};
            document.getElementById("reopenButton").onclick=function(){reopenTask(id);};
            if(task.getElementsByTagName("status")[0].childNodes[0].nodeValue!=2){
                $('#reopenButton').hide();
            } else{
                $('#titleChngBtn').hide();
                $('#descChngBtn').hide();
                $('#locChngBtn').hide();
                $('#userChngBtn').hide();
                $('#typeChngBtn').hide();
                $('#dateChngBtn').hide();
                $('#timeChngBtn').hide();
            }
        }
    }
}

/* Sends the desired modifications to the back-end. */
function modifyTask(what, where,id){
    if(what=="description"){
        var how =document.getElementById(where).innerHTML;
    } else{
        var how = document.getElementById(where).value;
    }
    if(how.length>0){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/Modify/"+id+"/"+what+"/"+how+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = taskModCallback;
    req.send(null);
    }
}

/* Time has its own version since it is more convenient to handle this way. */
function modifyTaskTime(id){
    var how = document.getElementById("dueDate").value+"T"+document.getElementById("dueTime").value;
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/Modify/"+id+"/datetime/"+how+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = taskModCallback;
    req.send(null);
}

/* Task removal request to the server. */
function removeTask(id){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/Remove/"+id+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = taskModCallback;
    req.send(null);
}

/* Request to the server to reopen the selected task. */
function reopenTask(id){
    var url= "http://localhost:8080/PrismaVirtualNoteboard/webresources/Tasks/Reopen/"+id+"/"+localStorage.getItem("sessionId");
    req=initRequest();
    req.open("PUT",url,true);
    req.onreadystatechange = taskModCallback;
    req.send(null);
}