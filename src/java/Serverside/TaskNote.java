/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Serverside;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jarno
 */

@XmlRootElement
@Entity
public class TaskNote implements Serializable {
    private LocalDateTime crtTime; //for sorting tasks in order of creation
    private String creator;
    private int id;
    private String taskTitle;
    private String taskDescFull;
    private String location;
    private int status; //0=open,1=underway,2=complete
    private LocalDateTime checkout; //when the task was checked out by a person
    private String tUser;//the user working on the task
    private int checkoutTimeout =45; //in minutes, as determined in checkTimeout() implementation
    private String tasktype; //what type of employee can take the task, ex. cashier, shelver
    private LocalDateTime dueDate; //can be created through: LocalDateTime.parse(""yyyy-mm-ddThh:mm");, ex."2007-12-03T10:15"  in TaskResource create/modify
    
    public TaskNote(String creatorUname, String tasktitle, String descFull, String location,String tasktype,String dueBy){
        this.crtTime = LocalDateTime.now();
        this.creator = creatorUname;
        this.taskTitle=tasktitle;
        this.taskDescFull=descFull;
        this.location=location;
        this.tUser = null;
        this.status=0;
        this.checkout= null;
        this.tasktype=tasktype;
        this.id=hashCode();
        this.dueDate= LocalDateTime.parse(dueBy);
    }
    
    public TaskNote(){
        this("","","","","","2020-11-22T10:15");
    }
    
    /*Allows an user to check out a task for themselves provided it is not yet full*/
    public String checkOut(AppUser checker){
        if(this.status ==0){
            this.status=1;
            this.tUser=checker.getUserName();
            this.checkout=LocalDateTime.now();
            return this.checkout.toString();
        }
        return "no";
    }
    
    /* This ended up going unused. Could add a timeout check to all task list requests put that would probably introduce noticeable slowdown. */
    public void checkTimeout(){
        if(this.status==1){
            if(this.checkout.plusMinutes(this.checkoutTimeout).compareTo( LocalDateTime.now())<0){ //if a checkout is older than the assigned limit, it is removed
                this.checkout=null;
                this.tUser=null;
                this.status=0;
            }
        }
    }
    
    /*The reverse of checking out a task, this removes the user from the list of people undertaking the task (if present)
      as well as setting the status back to 'untaken' if no users remain 'on task'.*/
    public void removeCheckOut(){
        if(this.status==1){
            this.checkout=null;
            this.tUser=null;
            this.status=0;    
        }
    }
    
    /*From here it is getters & setters plus a hashCode() implementation*/
    
    @XmlElement
    @Basic
    public String getCreationTime(){
        return this.crtTime.toString();
    }
    
    @XmlElement
    @Basic
    public String getCreator(){
        return this.creator;
    }
    
    @XmlElement
    @Basic
    public String getTaskTitle(){
        return this.taskTitle;
    }
    
    @XmlElement
    @Basic
    public String getTaskDescF(){
        return this.taskDescFull;
    }
    
    @XmlElement
    @Basic
    public String getLocation(){
        return this.location;
    }
    
    @XmlElement
    @Basic
    public int getStatus(){
        return this.status;
    }
    
    @XmlElement
    @Basic
    public String getCheckout(){
        //checkTimeout();
        if(this.checkout!=null){
            String time = this.checkout.toString();
            return ""+time;
        }
        return null;
    }
    
    @XmlElement
    @Basic
    public String getUser(){
        //checkTimeout();
        if(this.tUser!=null){
            return this.tUser;
        }
        return null;
    }
    
    @XmlElement
    @Basic
    public String getTaskType(){
        return this.tasktype;
    }
    
    @XmlElement
    @Basic
    public String getDueDate(){
        return this.dueDate.toString();
    }
    
    @XmlElement
    @Id
    public int getId(){
        return this.id;
    }
    
    public void setCreationTime(String timeStr){
        LocalDateTime ctime = LocalDateTime.parse(timeStr);
        this.crtTime=ctime;
    }
    
    public void setCreator(String creator){
        this.creator=creator;
    }
    
    public void setTaskTitle(String title){
        this.taskTitle=title;
    }
    
    public void setTaskDescF(String descF){
        this.taskDescFull=descF;
    }
    
    public void setLocation(String loc){
        this.location=loc;
    }
    
    public void setStatus(int status){
        this.status=status;
    }
    
    public void setCheckout(String checkout){
        if(checkout!=null){
        LocalDateTime ctime = LocalDateTime.parse(checkout);
        this.checkout=ctime;
        }
        this.checkout=null;
    }
    
    public void setUser(String user){
        this.tUser=user;
    }
    
    public void setTaskType(String tasktype){
        this.tasktype=tasktype;
    }
    
    public void setDueDate(String timeStr){
        LocalDateTime ctime = LocalDateTime.parse(timeStr);
        this.dueDate=ctime;
    }
    
    public void setId(int id){
        this.id=id;
    }
    
    /* This was originally based off creator + creation time. 
    That would work fine in actual use but caused errors when populating the back-end for testing, 
    since the tasks were created at the same time by the same person. 
    Task description is long enough to have the variety to ensure that two task notes are very unlikely receive the same id.*/
    @Override
    public int hashCode(){
        int hash=1;
        hash=hash*23+taskDescFull.hashCode();
        hash=hash*7+creator.hashCode();
        hash=hash*13+taskTitle.hashCode();
        hash=hash*17+location.hashCode();
        return hash;
    }
}
