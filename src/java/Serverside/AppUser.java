/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Serverside;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jarno
 */

@XmlRootElement
@Entity
public class AppUser implements Serializable {
    private String userName;
    private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String phone;
    private Set<String> jobs;//jobclass(es?) as a set
    private int accessLevel; //level of access to App functions, ex. ability to create tasks: -1=closed account, 1=normal user, 2=manager
    
    public AppUser(String fname, String lname, String uname, String psw, String email, String phone, int access, Set<String> jobs){
        this.firstName=fname;
        this.lastName=lname;
        this.userName=uname;
        this.password=psw;
        this.email=email;
        this.phone=phone;
        this.accessLevel=access;
        this.jobs=jobs;
    }
    
    public AppUser(){
        this("","","","","","",1,new HashSet<String>());
    }
    
    /*From here it is getters & setters plus a hashCode() implementation*/
    
    @XmlElement
    @Id
    public String getUserName(){
        return this.userName;
    }
    
    @XmlElement
    @Basic
    public String getFirstName(){
        return this.firstName;
    }
    
    @XmlElement
    @Basic
    public String getLastName(){
        return this.lastName;
    }
    
    @XmlElement
    @Basic
    public String getEmail(){
        return this.email;
    }
    
    @XmlElement
    @Basic
    public String getPhone(){
        return this.phone;
    }
    
    @XmlElement
    @Basic
    public String getPassword(){
        return this.password;
    }
    
    @XmlElement
    @ElementCollection(fetch=FetchType.EAGER)
    public Set<String> getJobs(){
        return this.jobs;
    }
    
    @XmlElement
    @Basic
    public int getAccess(){
        return this.accessLevel;
    }
    
    
    public void setUserName(String uname){
        this.userName=uname;
    }
    
    public void setFirstName(String fname){
        this.firstName=fname;
    }
    
    public void setLastName(String lname){
        this.lastName=lname;
    }
    
    public void setPassword(String psw){
        this.password =psw;
    }
    
    public void setEmail(String email){
        this.email =email;
    }
    
    public void setPhone(String phone){
        this.phone =phone;
    }
    
    public void setJobs(Set<String> jobs){
        this.jobs =jobs;
    }
    
    public void setAccess(int access){
        this.accessLevel = access;
    }
    
    /* User addition in UserResource checks to make sure no two users have the same username. */
    @Override
    public int hashCode(){
        int hash=1+13*this.userName.hashCode();
        return hash;
    }
}
