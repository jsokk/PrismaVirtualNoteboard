/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import Serverside.LoginManager;
import Serverside.TaskNote;
import Serverside.AppUser;
import Util.HibernateStuff;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.hibernate.Session;

/**
 *
 * @author Jarno
 */

@Path("/Tasks")
public class TaskResource {
    LoginManager LogMan = LoginManager.getInstance();
    
    /* Returns all tasks. Did not actually see use in the product since the tasks go into different divisions depending on their status.
    Saw some use in initial testing. */
    @Path("/All/{sessionId}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<TaskNote> getTasks(@PathParam("sessionId") String sessionId){
        if(LogMan.CheckSession(sessionId)){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();

        List<TaskNote> tasks = session.createCriteria(TaskNote.class).list();
        
        session.getTransaction().commit();
        return tasks;
        }
        return null;
    }
    
    /*Another one that did not see use for similar reasons, created in anticipation of potential need, 'just in case'. */
    @Path("/ForMe/All/{sessionId}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<TaskNote> myJobAll(@PathParam("sessionId") String sessionId){
        if(LogMan.CheckSession(sessionId)){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        AppUser crnt = LogMan.getBySesId(sessionId);
        List<TaskNote> tasks = new ArrayList<TaskNote>();
        List<TaskNote> tasksTemp = session.createCriteria(TaskNote.class).list();
        for(TaskNote t:tasksTemp){
            if(crnt.getJobs().contains(t.getTaskType())){
                tasks.add(t);
            }
        }
        session.getTransaction().commit();
        return tasks;
        }
        return null;
    }
    
    /* Regular users have their tasks retrieved by this. */
    @Path("/ForMe/ByStatus/{status}/{sessionId}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<TaskNote> myJobOpen(@PathParam("sessionId") String sessionId,@PathParam("status") String statusStr){
        if(LogMan.CheckSession(sessionId)){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        
        AppUser crnt = LogMan.getBySesId(sessionId);
        int status = Integer.parseInt(statusStr);
        
        List<TaskNote> tasks = new ArrayList<TaskNote>();
        List<TaskNote> tasksTemp = session.createCriteria(TaskNote.class).list();
        for(TaskNote t:tasksTemp){
            if(crnt.getJobs().contains(t.getTaskType()) && t.getStatus()==status){
                tasks.add(t);
            }
        }
        
        session.getTransaction().commit();
        return tasks;
        }
        return null;
    }
    
    /* Another anticipatory dud. */
    @Path("/ByJob/{jobs}/{sessionId}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<TaskNote> byJobs(@PathParam("sessionId") String sessionId,@PathParam("jobs") String jobStr){
        if(LogMan.CheckSession(sessionId)){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        
        Set<String> jobs = new HashSet<String>(Arrays.asList(jobStr.split("&")));
        
        List<TaskNote> tasks = new ArrayList<TaskNote>();
        List<TaskNote> tasksTemp = session.createCriteria(TaskNote.class).list();
        for(TaskNote t:tasksTemp){
            if(jobs.contains(t.getTaskType())){
                tasks.add(t);
            }
        }
        session.getTransaction().commit();
        return tasks;
        }
        return null;
    }
    
    /* The other in-use retrieval call. Managers get their task lists through this since they need to see all of them, not just the ones they are qualified to handle. */
    @Path("/ByStatus/{stat}/{sessionId}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<TaskNote> byStatus(@PathParam("sessionId") String sessionId,@PathParam("stat") String statStr){
        if(LogMan.CheckSession(sessionId)){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        
        int stat = Integer.parseInt(statStr);
        List<TaskNote> tasks = new ArrayList<TaskNote>();
        List<TaskNote> tasksTemp = session.createCriteria(TaskNote.class).list();
        for(TaskNote t:tasksTemp){
            if(t.getStatus()==stat){
                tasks.add(t);
            }
        }
        session.getTransaction().commit();
        return tasks;
        }
        return null;
    }
    
    /* Checkout of a task is an 'i will do this' indicator, removing the task from the pool of open tasks. */
    @Path("/Checkout/{taskId}/{sessionId}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String checkoutTask(@PathParam("sessionId") String sessionId,@PathParam("taskId") String taskIdStr){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        if(LogMan.CheckSession(sessionId)){
            int taskId = Integer.parseInt(taskIdStr);
            List<TaskNote> tasks = session.createCriteria(TaskNote.class).list();
            for(TaskNote t:tasks){
                if(t.getId()==taskId){
                    AppUser user = LogMan.getBySesId(sessionId);
                    String b= t.checkOut(user);
                    session.saveOrUpdate(t);
        
                    session.getTransaction().commit();
                    return "CHECKOUT SUCCESSFUL"+b;                
                }
            }
            session.getTransaction().commit();
            return "TASK NOT FOUND";
        }
        session.getTransaction().commit();
        return "SESSION EXPIRED";
    }
    
    /* Removes the checkout and returns the task to the 'To Do' pool where someone else can pick it up. */
    @Path("/Drop/{taskId}/{sessionId}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String dropTask(@PathParam("sessionId") String sessionId,@PathParam("taskId") String taskIdStr){
        if(LogMan.CheckSession(sessionId)){
            Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
            session.beginTransaction();
            int taskId = Integer.parseInt(taskIdStr);
            List<TaskNote> tasks = session.createCriteria(TaskNote.class).list();
            for(TaskNote t:tasks){
                if(t.getId()==taskId){
                    t.removeCheckOut();
                    session.saveOrUpdate(t);
        
                    session.getTransaction().commit();
                    return "TASK DROPPED";                
                }
            }
            session.getTransaction().commit();
            return "TASK NOT FOUND";
        }
        return "SESSION EXPIRED";
    }
    
    /* Self-explanatory. */
    @Path("/Complete/{taskId}/{sessionId}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String completeTask(@PathParam("sessionId") String sessionId,@PathParam("taskId") String taskIdStr){
        if(LogMan.CheckSession(sessionId)){
            Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
            session.beginTransaction();
            int taskId = Integer.parseInt(taskIdStr);
            List<TaskNote> tasks = session.createCriteria(TaskNote.class).list();
            for(TaskNote t:tasks){
                if(t.getId()==taskId){
                    t.setStatus(2);
                    session.saveOrUpdate(t);
        
                    session.getTransaction().commit();
                    return "TASK COMPLETED";                
                }
            }
            session.getTransaction().commit();
            return "TASK NOT FOUND";
        }
        return "SESSION EXPIRED";
    }
    
    /* Removes the 'competed' status of the task, returning it into an 'In Progress' task for the user that checked it out. */
    @Path("/Reopen/{taskId}/{sessionId}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String reopenTask(@PathParam("sessionId") String sessionId,@PathParam("taskId") String taskIdStr){
        if(LogMan.CheckSession(sessionId) && LogMan.getBySesId(sessionId).getAccess()>=2){
            Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
            session.beginTransaction();
            int taskId = Integer.parseInt(taskIdStr);
            List<TaskNote> tasks = session.createCriteria(TaskNote.class).list();
            for(TaskNote t:tasks){
                if(t.getId()==taskId){
                    if(t.getUser()!=null){
                        t.setStatus(1);
                    } else {
                        t.setStatus(0);
                    }
                    session.saveOrUpdate(t);
        
                    session.getTransaction().commit();
                    return "TASK REOPENED";                
                }
            }
            session.getTransaction().commit();
            return "TASK NOT FOUND";
        }
        return "SESSION EXPIRED";
    }
    
    /* Deletes a task. */
    @Path("/Remove/{taskId}/{sessionId}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String remove(@PathParam("sessionId") String sessionId,@PathParam("taskId") String taskIdStr){
        if(LogMan.CheckSession(sessionId) && LogMan.getBySesId(sessionId).getAccess()>=2){
            Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
            session.beginTransaction();
            int taskId = Integer.parseInt(taskIdStr);
            List<TaskNote> tasks = session.createCriteria(TaskNote.class).list();
            for(TaskNote t:tasks){
                if(t.getId()==taskId){
                    session.delete(t);
        
                    session.getTransaction().commit();
                    return "TASK REMOVED";                
                }
            }
            session.getTransaction().commit();
            return "TASK NOT FOUND";
        }
        return "SESSION EXPIRED";
    }
    
    /* Allows fully modifying the task, including assigning it to a qualified user directly and/or removing the checkout by another users from it. */
    @Path("/Modify/{taskId}/{what}/{how}/{sessionId}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String modifyTask(@PathParam("sessionId") String sessionId,@PathParam("taskId") String taskIdStr,@PathParam("what") String taskParam,@PathParam("how") String taskMod){
        if(LogMan.CheckSession(sessionId)){
            if(LogMan.getBySesId(sessionId).getAccess()>=2){
                Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
                session.beginTransaction();
                int taskId = Integer.parseInt(taskIdStr);
                List<TaskNote> tasks = session.createCriteria(TaskNote.class).list();
                for(TaskNote t:tasks){
                    if(t.getId()==taskId){
                        if(taskParam.equals("title")){
                            t.setTaskTitle(taskMod);
                        } else if(taskParam.equals("description")){
                            t.setTaskDescF(taskMod);
                        } else if(taskParam.equals("location")){
                            t.setLocation(taskMod);
                        } else if(taskParam.equals("user")){
                            t.removeCheckOut();
                            if(!taskMod.equals("none")){
                                List<AppUser> users =session.createCriteria(AppUser.class).list();
                                for(AppUser u:users){
                                    if(u.getUserName().equals(taskMod)){
                                        t.checkOut(u);
                                    }
                                }
                            }
                        } else if(taskParam.equals("tasktype")){
                            t.setTaskType(taskMod);
                        } else if(taskParam.equals("datetime")){
                            t.setDueDate(taskMod);
                        } else {
                            session.getTransaction().commit();
                            return "PARAM NOT RECONGNIZED";     
                        }
                        session.saveOrUpdate(t);
                        session.getTransaction().commit();
                        return "TASK MODIFIED";                
                    }
                }
                session.getTransaction().commit();
                return "TASK NOT FOUND";
            }
            return "ACCESS DENIED";
        }
        return "SESSION EXPIRED";
    }
    
    /* Allows creating new tasks. Manager-only. */
    @Path("/Create/{title}/{desc}/{loc}/{tasktype}/{dueby}/{sessionId}") //dueby in format: "yyyy-mm-ddThh:mm"
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String createTasks(@PathParam("sessionId") String sessionId,@PathParam("title") String title,@PathParam("desc") String desc,@PathParam("loc") String loc,@PathParam("tasktype") String tasktype,@PathParam("dueby") String dueby){
        if(LogMan.CheckSession(sessionId)){
            AppUser crnt = LogMan.getBySesId(sessionId);
            if(crnt.getAccess()>=2){
                Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
                session.beginTransaction();
                
                TaskNote newtask = new TaskNote(crnt.getUserName(),title,desc,loc,tasktype,dueby); 
                session.saveOrUpdate(newtask);
        
                session.getTransaction().commit();
                return "SUCCESS";
            }
            return "DENIED";
        }
        return "SESSION EXPIRED";
    }
    
    /* Creates a set of tasknotes for testing purposes. Can also reset them to these initial values. */
    @Path("/populate/{force}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String populate(@PathParam("force") String force){
        /* creates a bunch of filler tasks for testing purposes */
        if(LogMan.getTaskPop() || force.equals("force")){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        String sendback ="Populate failed - no suitable users to designate as task creators.";
        
        //content here
        List<AppUser> users = new ArrayList<AppUser>();//session.createCriteria(User.class).add(Restrictions.gt("accessLevel", 2)).list();
        
        Set<String> jobsBob = new HashSet<String>();
        jobsBob.add("manager");
        jobsBob.add("freshfoods");
        jobsBob.add("freshfoods");
        AppUser bob = new AppUser("Bob", "Stein", "BobStei", "greenisgood","bstein@prisma.fi","555 5555 555", 2, jobsBob);
        users.add(bob);
        
        Random rng = new Random();
        int intUser  =0;
        
        if(users.size()>0){
        ArrayList<TaskNote> popTasks = new ArrayList<TaskNote>();
        String desc="Customer dropped  milk carton, spilled all over the floor.";
        TaskNote task = new TaskNote(users.get(intUser).getUserName(),"Cleanup",desc,"Aisle 6","cleaner","2020-12-03T10:15"); 
        popTasks.add(task);
        
        intUser  =rng.nextInt(users.size());
        desc="Our flagship product 'hilavitkutin' has run out from the shelves, restock ASAP.";
        task = new TaskNote(users.get(intUser).getUserName(),"Restock",desc,"Aisle 13","shelver","2020-12-03T10:15"); 
        popTasks.add(task);
        
        intUser  =rng.nextInt(users.size());
        desc="Check the bread department to ensure that no product overstays its 'best by' date.";
        task = new TaskNote(users.get(intUser).getUserName(),"Check expiration",desc,"Aisle 3","shelver","2020-12-03T10:15"); 
        popTasks.add(task);
        
        intUser  =rng.nextInt(users.size());
        desc="Checkout lines are growing too long, open a register.";
        task = new TaskNote(users.get(intUser).getUserName(),"Open register",desc,"Register 9","cashier","2020-12-03T10:15"); 
        popTasks.add(task);
        
        intUser  =rng.nextInt(users.size());
        desc="Check the meat product 'best by' dates and put the usual 30% discount on any that need to go today.";
        task = new TaskNote(users.get(intUser).getUserName(),"Apply discount labels",desc,"Aisle 42","shelver","2020-12-03T10:15"); 
        popTasks.add(task);
        
        intUser  =rng.nextInt(users.size());
        desc="Cashier at register 6 needs a break, go switch with them.";
        task = new TaskNote(users.get(intUser).getUserName(),"Switch with cashier",desc, "Register 6", "cashier","2020-12-03T10:15"); 
        popTasks.add(task);
        
        intUser  =rng.nextInt(users.size());
        desc="We need fresh bread for the day.";
        task = new TaskNote(users.get(intUser).getUserName(),"Bake bread",desc, "Bread Aisle", "baker","2020-12-03T10:15"); 
        popTasks.add(task);
        
        intUser  =rng.nextInt(users.size());
        desc="Check the greenery aisle for any wilted or otherwise damaged goods.";
        task = new TaskNote(users.get(intUser).getUserName(),"Check product",desc, "Greenery Aisle", "freshfoods","2020-12-03T10:15"); 
        popTasks.add(task);
        
        
        for(TaskNote t: popTasks) {
            session.saveOrUpdate(t);
            }
        
        sendback="Tasks populated successfully.";
        }
        LogMan.userPop();
        session.getTransaction().commit();
        return sendback;
        }
        return "NO POP FOR YOU";
    }
    
    
    
    
}
