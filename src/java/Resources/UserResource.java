/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import Serverside.LoginManager;
import Serverside.AppUser;
import Util.HibernateStuff;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.hibernate.Criteria;
import org.hibernate.Session;

/**
 *
 * @author Jarno
 */


@Path("/Users")
public class UserResource {
    LoginManager LogMan = LoginManager.getInstance();
    
    /* Adds an user to the system. If the would-be username already is in use the new user is not created. */
    @Path("/Add/{fname}/{lname}/{uname}/{psw}/{email}/{phone}/{access}/{jobs}/{sessionId}")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String addUser(@PathParam("sessionId") String sessionId,@PathParam("fname") String fname,@PathParam("lname") String lname,@PathParam("uname") String uname,@PathParam("psw") String psw,@PathParam("phone") String phone,@PathParam("email") String email,@PathParam("access") String access,@PathParam("jobs") String jobStr){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        updateLogins();
        if(LogMan.CheckSession(sessionId)){
        
            List<AppUser> users = session.createCriteria(AppUser.class).list();
            for(AppUser u:users){
                if(u.getUserName().equals(uname)){
                    session.getTransaction().commit();
                    return "USERNAME ALREADY IN USE";
                }
            }
            Set<String> jobs = new HashSet<String>(Arrays.asList(jobStr.split("&")));
        
            AppUser newuser = new AppUser(fname, lname, uname, psw,email,phone, Integer.parseInt(access), jobs); //In order: first name, last name, user name, password, access lvl, jobs
            session.saveOrUpdate(newuser);
        
            session.getTransaction().commit();
            return "SUCCESS";
        }
        return "SESSION EXPIRED";
    }
    
    /* Used to retrieve the data of an user with their username. */
    @Path("/View/{uname}/{sessionId}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public AppUser viewInfo(@PathParam("sessionId") String sessionId, @PathParam("uname") String uname){
        if(LogMan.CheckSession(sessionId)){
            Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
            session.beginTransaction();
        
            List<AppUser> users = session.createCriteria(AppUser.class).list();
            for(AppUser u:users){
                if(u.getUserName().equals(uname) && u.getAccess()<3) {
                    session.getTransaction().commit();
                    return u;
                }
            }
            session.getTransaction().commit();
        }
        return null;
    }
    
    /* Uses the Login Manager to retrieve the data for the user logged in. */
    @Path("/View/Myself/{sessionId}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public AppUser viewMyInfo(@PathParam("sessionId") String sessionId){
        if(LogMan.CheckSession(sessionId)){
            AppUser crnt = LogMan.getBySesId(sessionId);
            return crnt;
        }
        return null;
    }
    
    /* Retrieves the data of all users. Limited to manager-level users. 
    'master admin' is not included as regular managers are not premitted to modify or view that account. */
    @Path("/View/All/{sessionId}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<AppUser> viewInfoAll(@PathParam("sessionId") String sessionId){
        AppUser crnt = LogMan.getBySesId(sessionId);
        if(LogMan.CheckSession(sessionId) && crnt.getAccess()>=2){
            Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
            session.beginTransaction();
            List<AppUser> users = session.createCriteria(AppUser.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
            AppUser toBeRemoved=null;
            for(AppUser u:users){
                if(u.getAccess()==3){
                    toBeRemoved =u;
                }
            }
            users.remove(toBeRemoved);
            session.getTransaction().commit();
            return users;            
        }
        return null;
    }
    
    /* Changes user attributes. This one is accessible to regular users while the one below it is for managers only. */
    @Path("/Profile/EditProfileSelf/{what}/{how}/{sessionId}") //these can be changed by the user themselves
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String changeMyAttr(@PathParam("sessionId") String sessionId,@PathParam("what") String targetAtt,@PathParam("how") String newAttr){
        if(LogMan.CheckSession(sessionId)){
            AppUser crnt = LogMan.getBySesId(sessionId);
                Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
                session.beginTransaction();
                if(crnt.getAccess()==3){
                    return "DENIED";
                }
                if(targetAtt.equals("email")){
                    crnt.setEmail(newAttr);
                
                } else if(targetAtt.equals("phone")){
                    crnt.setPhone(newAttr);
                
                } else if(targetAtt.equals("psw")){
                    crnt.setPassword(newAttr);
                    updateLogins();
                } else {
                    session.getTransaction().commit();
                    return "UNKNOWN ATTRIBUTE";
                }
                session.saveOrUpdate(crnt);
                
                session.getTransaction().commit();
                return "SUCCESS";
        }
        return "SESSION EXPIRED";
    }
    
    
    @Path("/Profile/EditProfileOther/{who}/{what}/{how}/{sessionId}") ///these can only be changed by managers, for themselves or others. Job lists used to be handled separately.
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String changeUserAttr(@PathParam("sessionId") String sessionId,@PathParam("who") String uName,@PathParam("what") String targetAtt,@PathParam("how") String newAttr){
        if(LogMan.CheckSession(sessionId)){
            AppUser crnt = LogMan.getBySesId(sessionId);
            if(crnt.getAccess()>=2){
                Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
                session.beginTransaction();
                List<AppUser> users = session.createCriteria(AppUser.class).list();
                for(AppUser u:users){
                    if(u.getUserName().equals(uName) && u.getAccess()<3) { //editing the 'master admin' is prevented
                        
                        if(targetAtt.equals("fname")){
                            u.setFirstName(newAttr);
                        } else if(targetAtt.equals("lname")){
                            u.setLastName(newAttr);
                
                        } else if(targetAtt.equals("uname")){
                            for(AppUser uu:users){
                                if(uu.getUserName().equals(u.getUserName())){
                                    session.getTransaction().commit();
                                    return "USERNAME TAKEN";
                                }
                             }
                        } else if(targetAtt.equals("psw")){
                            u.setPassword(newAttr);
                            updateLogins();
                        } else if(targetAtt.equals("access")){
                            u.setAccess(Integer.parseInt(newAttr));
                            Set<String> oldjobs=u.getJobs();
                            if(Integer.parseInt(newAttr)==2){
                                oldjobs.add("manager");
                            } else {
                                oldjobs.remove("manager");
                            }
                            u.setJobs(oldjobs);
                        } else if(targetAtt.equals("jobs")){
                            Set<String> jobs = new HashSet<String>(Arrays.asList(newAttr.split("&")));
                            if(jobs.contains("manager")){
                                u.setAccess(2);
                            } else {
                                u.setAccess(1);
                            }
                            u.setJobs(jobs);
                        } else if(targetAtt.equals("email")){
                            u.setEmail(newAttr);
                        } else if(targetAtt.equals("phone")){
                            u.setPhone(newAttr);
                        } else {
                            session.getTransaction().commit();
                            return "UNKNOWN ATTRIBUTE";
                        }
                        session.saveOrUpdate(u);
                        session.getTransaction().commit();
                        return "SUCCESS";
                    }
                }
                
                session.getTransaction().commit();
                return "USER NOT FOUND";
            }
            return "DENIED";
        }
        return "SESSION EXPIRED";
    }
    
    /*Old job modification system was made before the UI was considered. Integrating it to the above turned out to be more convenient. */
    
    /* 
    Call this service whenever moving between pages to determine that the user still has a valid session
    If this returns 'FALSE', kick user back to login screen instead.
    */
    @Path("/Check/{sessionId}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String checkIn(@PathParam("sessionId") String sessionId){
        updateLogins();
        if(LogMan.CheckSession(sessionId)){
        return "TRUE";
        }
        return "FALSE";
    }
    
    /* 
    A successful login returns a non-null 32 char sesId String, a failure returns 'FAILURE'.
    Store the session id so that it persists when moving between pages, since you need it to sign all your REST calls.
    */
    @Path("/Login/{uname}/{psw}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String login(@PathParam("uname") String uname,@PathParam("psw") String psw){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        updateLogins();
        String sesId = LogMan.Login(uname, psw);
        session.getTransaction().commit();
        return sesId;
    }
    
    /* Removes the submitted sessionId from the maps tracking valid sessionIds. */
    @Path("/Logout/{sessionId}")
    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    public String logout(@PathParam("sessionId") String sessionId){          
        return LogMan.Logout(sessionId);
        }
    
    /* Creates a set of users for testing and demonstrating the application. If called again, those users are reset to this state due to using 'SaveOrUpdate'. 
    Plain one-time setup is done with just '/populate/' while appending 'force' forces a reset. */
    @Path("/populate/{force}")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String populate(@PathParam("force") String force){
        /* creates a bunch of filler users (based off the user profiles already made) for app testing purposes */
        if(LogMan.getUserPop()|| force.equals("force")){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        String sendback = "Users successfully populated.";
        
        ArrayList<AppUser> popUsers = new ArrayList<AppUser>();
        Set<String> jobsTimmy = new HashSet<String>();
        jobsTimmy.add("any");
        jobsTimmy.add("cashier");
        Set<String> jobsJohnny = new HashSet<String>();
        jobsJohnny.add("any");
        jobsJohnny.add("shelver");
        Set<String> jobsJim = new HashSet<String>();
        jobsJim.add("any");
        jobsJim.add("baker");
        jobsJim.add("cashier");
        Set<String> jobsBob = new HashSet<String>();
        jobsBob.add("any");
        jobsBob.add("manager");
        jobsBob.add("cashier");
        Set<String> jobsBill = new HashSet<String>();
        jobsBill.add("any");
        jobsBill.add("manager");
        jobsBill.add("baker");
        Set<String> jobsJack = new HashSet<String>();
        jobsJack.add("any");
        jobsJack.add("manager");
        jobsJack.add("shelver");
        jobsJack.add("butcher");
        Set<String> jobsWenhao = new HashSet<String>();
        jobsWenhao.add("any");
        jobsWenhao.add("butcher");
        jobsWenhao.add("shelver");
        Set<String> TrueAdmin = new HashSet<String>();
        TrueAdmin.add("omniscience");
        TrueAdmin.add("omnipotence");
        TrueAdmin.add("omnipresence");
        AppUser TAd = new AppUser("True", "Administrator", "Sel1oSa1inSankari", "1mKbElcHIGQTg","TrueAdmin@Team1.com","555 3104 2838", 3, TrueAdmin);
        AppUser tim = new AppUser("Timmy", "Russell", "TimRuss", "drowssap","truss@prisma.fi","555 5555 555", 1, jobsTimmy);
        AppUser jon = new AppUser("Johnny", "Doe", "JohnDoe", "swordfish","jdoe@prisma.fi","555 5555 555", 1, jobsJohnny);
        AppUser jim = new AppUser("Jim", "Masters", "JimMast", "putzle","jmasters@prisma.fi","555 5555 555", 1, jobsJim);
        AppUser bob = new AppUser("Bob", "Stein", "BobStei", "greenisgood","bobstein@prisma.fi","555 5555 555", 2, jobsBob);
        AppUser bil = new AppUser("Bill", "Stein", "BillSte", "filigree","bilstein@prisma.fi","555 5555 555", 2, jobsBill);
        AppUser jac = new AppUser("Jack", "Quick", "JackQui", "swift","jquick@prisma.fi","555 5555 555", 2, jobsJack);
        AppUser wen = new AppUser("Wenhao", "Liang", "WeLiang", "security","wliang@prisma.fi","555 5555 555", 1, jobsWenhao);
        popUsers.add(tim);
        popUsers.add(jon);
        popUsers.add(jim);
        popUsers.add(bob);
        popUsers.add(bil);
        popUsers.add(jac);
        popUsers.add(wen);
        popUsers.add(TAd);
        
        for(AppUser u: popUsers) {
            session.saveOrUpdate(u);
        }
        LogMan.taskPop();
        session.getTransaction().commit();
        return sendback;
        }
        return "NO POP FOR YOU";
    }
    
    /* Passes the current user data to the Login Manager so it can replace the list of valid logins with the most up-to-date one. */
    private void updateLogins(){
        Session session = HibernateStuff.getInstance().getSessionFactory().openSession();
        session.beginTransaction();
        
        List<AppUser> users = session.createCriteria(AppUser.class).list();
        LogMan.UpdateLogins(users);
        
        session.getTransaction().commit();
    }
}
